/* 

it is a c console program which 
- computes external parameter ray 
- uses arbitrary precision ( mpfr) with dynamic precision adjustment
- uses Newton method ( described by T Kawahira) http://www.math.nagoya-u.ac.jp/~kawahira/programs/mandel-exray.pdf



gcc  e.c -std=c99 -Wall -Wextra -pedantic -lmpfr -lgmp -lm 

 ./rayin 1/3 25
 ./rayin 1/3 25 >ray13.txt
 ./rayin 1/3 25  | ./rescale 53 53 -0.75 0 1.5 0 >rray13.txt



 program uses the code by by Claude Heiland-Allen
 from https://gitorious.org/maximus/book/


dwell = integer escape time
dwell band = level set of integer escape time 

" ray_in computes 4 points per dwell band, 

every point on a ray_in trace is exterior (you'd need an infinite number 
of steps to reach the boundary), so these two cases are to be expected - 
zooming in on the ray end point isn't the same as zooming in on the 
theoretical landing point of the ray.


and ray_out currently computes 16.  
 Moreover ray_in has dynamic precision adjustment and 
ray_out doesn't (so you need a high precision to get through the thin 
gaps) - fixing both of these is on my mental todo list.  ray_out will 
always be slower though, as it needs to compute extra points when 
crossing dwell bands to accumulate angle bits (and to work correctly)."

What algorithm do you use for drawing external ray ?

essentially the Newton's method one in
http://www.math.nagoya-u.ac.jp/~kawahira/programs/mandel-exray.pdf

precision is automatic, effectively it checks how close dwell bands are 
together and uses higher precision when they are narrow and lower 
precision when they are wide.  in general precision will increase along 
the ray as it gets closer to the boundary. 


periodic rays stop near the landing point at the current zoom level, 
manually retrace if you zoom in and find the gap annoying.  tracing 
periodic rays is a lot slower than tracing preperiodic rays, especially 
if you zoom in to the cusp - suggestions welcome for improvements here 
(perhaps a larger radius than a single pixel for landing point nearness 
threshold?).

actually it seems specific to the way the GUI traces rays (probably how 
it finds the landing point for periodic rays).


here's my debugging (added fprintf(stderr,..) for number of iterations 
and precision changes in lib/mandelbrot_external_ray_in.c):

$ time ./bin/mandelbrot_external_ray_in 1/3 1000 >/dev/null 2>ray-1-3.txt

real    0m5.376s
user    0m5.340s
sys     0m0.016s
$ time ./bin/mandelbrot_external_ray_in 1/2 1000 >/dev/null 2>ray-1-2.txt

real    0m27.071s
user    0m27.034s
sys     0m0.012s

tracing periodic ray to a given depth is actually much faster than 
pre-periodic!

$ cat ray-1-3.txt | grep -v ^P | sort | uniq -c
       4 1
      11 2
    3985 3
$ cat ray-1-2.txt | grep -v ^P | sort | uniq -c
       4 1
    1912 2
    2024 3

though pre-periodic ray uses fewer newton iterations on average (around 
2.5 instead of 3 at each step).

the reason why:

$ grep ^P ray-1-3.txt | tail -n 1
$ grep ^P ray-1-2.txt | tail -n 1
P1973

the pre-periodic ray increases precision much more, because the ray 
steps get much closer to each other, going up to 1973 bits - this is why 
it's so much slower

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gmp.h>
#include <mpfr.h> // arbitrary precision 





const double pi = 3.14159265358979323846264338327950288419716939937510;



// point of ray 
struct mandelbrot_external_ray_in {

  mpq_t angle;
  mpq_t one;
  unsigned int sharpness; // number of steps to take within each dwell band
  unsigned int precision; // delta needs this many bits of effective precision
  unsigned int accuracy;  // epsilon is scaled relative to magnitude of delta
  double escape_radius;
  mpfr_t epsilon2;
  mpfr_t cx;
  mpfr_t cy;
  unsigned int j;
  unsigned int k;
  // temporaries
  mpfr_t zx, zy, dzx, dzy, ux, uy, vx, vy, ccx, ccy, cccx, cccy;
};



// new : allocates memory, inits and sets variables
// input angle = external angle in turns 
extern struct mandelbrot_external_ray_in *mandelbrot_external_ray_in_new(mpq_t angle) {

  struct mandelbrot_external_ray_in *r = malloc(sizeof(struct mandelbrot_external_ray_in));

  mpq_init(r->angle);
  mpq_set(r->angle, angle);

  mpq_init(r->one);
  mpq_set_ui(r->one, 1, 1);

  r->sharpness = 4;
  r->precision = 4;
  r->accuracy  = 4;
  r->escape_radius = 65536.0;

  mpfr_init2(r->epsilon2, 53);
  mpfr_set_ui(r->epsilon2, 1, GMP_RNDN);
  // external angle a in radions
  double a = 2.0 * pi * mpq_get_d(r->angle);
  // initial value c = escape_radius * e^a
  mpfr_init2(r->cx, 53);
  mpfr_init2(r->cy, 53);
  mpfr_set_d(r->cx, r->escape_radius * cos(a), GMP_RNDN);
  mpfr_set_d(r->cy, r->escape_radius * sin(a), GMP_RNDN);

  r->k = 0;
  r->j = 0;
  // initialize temporaries
  mpfr_inits2(53, r->zx, r->zy, r->dzx, r->dzy, r->ux, r->uy, r->vx, r->vy, r->ccx, r->ccy, r->cccx, r->cccy, (mpfr_ptr) 0);
  return r;
}


// delete
extern void mandelbrot_external_ray_in_delete(struct mandelbrot_external_ray_in *r) {
  mpfr_clear(r->epsilon2);
  mpfr_clear(r->cx);
  mpfr_clear(r->cy);
  mpq_clear(r->angle);
  mpq_clear(r->one);
  mpfr_clears(r->zx, r->zy, r->dzx, r->dzy, r->ux, r->uy, r->vx, r->vy, r->ccx, r->ccy, r->cccx, r->cccy, (mpfr_ptr) 0);
  free(r);
}



/* step 
What algorithm do you use for drawing external ray ?
essentially the Newton's method one in http://www.math.nagoya-u.ac.jp/~kawahira/programs/mandel-exray.pdf

there are 2 planes :
* t-plane 
* c-plane 

*/
extern int mandelbrot_external_ray_in_step(struct mandelbrot_external_ray_in *r) {

  
  // check ????
  if (r->j >= r->sharpness) {
    mpq_mul_2exp(r->angle, r->angle, 1);
    if (mpq_cmp_ui(r->angle, 1, 1) >= 0) {
      mpq_sub(r->angle, r->angle, r->one);
    }
    r->k++;
    r->j = 0;
  }

  // initial radius r0 <- er ** ((1/2) ** ((j + 0.5)/sharpness))
  double r0 = pow(r->escape_radius, pow(0.5, (r->j + 0.5) / r->sharpness));
  // initial external angle a0
  double a0 = 2.0 * pi * mpq_get_d(r->angle); // change units from turn to radian
  // initial point on t-plane : t0 = r0 * e^(a0 * i)  = t0x + t0y*i 
  double t0x = r0 * cos(a0);
  double t0y = r0 * sin(a0);


  // c <- r->c
  mpfr_set(r->ccx, r->cx, GMP_RNDN);
  mpfr_set(r->ccy, r->cy, GMP_RNDN);


  for (unsigned int i = 0; i < 64; ++i) { // FIXME arbitrary limit
    // z <- 0
    // dz <- 0
    mpfr_set_ui(r->zx, 0, GMP_RNDN);
    mpfr_set_ui(r->zy, 0, GMP_RNDN);
    mpfr_set_ui(r->dzx, 0, GMP_RNDN);
    mpfr_set_ui(r->dzy, 0, GMP_RNDN);


    // iterate  to converge
    for (unsigned int p = 0; p <= r->k; ++p) {
      // dz <- 2 z dz + 1
      mpfr_mul(r->ux, r->zx, r->dzx, GMP_RNDN);
      mpfr_mul(r->uy, r->zy, r->dzy, GMP_RNDN);
      mpfr_mul(r->vx, r->zx, r->dzy, GMP_RNDN);
      mpfr_mul(r->vy, r->zy, r->dzx, GMP_RNDN);
      mpfr_sub(r->dzx, r->ux, r->uy, GMP_RNDN);
      mpfr_add(r->dzy, r->vx, r->vy, GMP_RNDN);
      mpfr_mul_2ui(r->dzx, r->dzx, 1, GMP_RNDN);
      mpfr_mul_2ui(r->dzy, r->dzy, 1, GMP_RNDN);
      mpfr_add_ui(r->dzx, r->dzx, 1, GMP_RNDN);
      // z <- z^2 + c
      mpfr_sqr(r->ux, r->zx, GMP_RNDN);
      mpfr_sqr(r->uy, r->zy, GMP_RNDN);
      mpfr_sub(r->vx, r->ux, r->uy, GMP_RNDN);
      mpfr_mul(r->vy, r->zx, r->zy, GMP_RNDN);
      mpfr_mul_2ui(r->vy, r->vy, 1, GMP_RNDN);
      mpfr_add(r->zx, r->vx, r->ccx, GMP_RNDN);
      mpfr_add(r->zy, r->vy, r->ccy, GMP_RNDN);
    }
    // c' <- c - (z - t0) / dz
    mpfr_sqr(r->ux, r->dzx, GMP_RNDN);
    mpfr_sqr(r->uy, r->dzy, GMP_RNDN);
    mpfr_add(r->vy, r->ux, r->uy, GMP_RNDN);
    mpfr_sub_d(r->zx, r->zx, t0x, GMP_RNDN);
    mpfr_sub_d(r->zy, r->zy, t0y, GMP_RNDN);
    mpfr_mul(r->ux, r->zx, r->dzx, GMP_RNDN);
    mpfr_mul(r->uy, r->zy, r->dzy, GMP_RNDN);
    mpfr_add(r->vx, r->ux, r->uy, GMP_RNDN);
    mpfr_div(r->ux, r->vx, r->vy, GMP_RNDN);
    mpfr_sub(r->cccx, r->ccx, r->ux, GMP_RNDN);
    mpfr_mul(r->ux, r->zy, r->dzx, GMP_RNDN);
    mpfr_mul(r->uy, r->zx, r->dzy, GMP_RNDN);
    mpfr_sub(r->vx, r->ux, r->uy, GMP_RNDN);
    mpfr_div(r->uy, r->vx, r->vy, GMP_RNDN);
    mpfr_sub(r->cccy, r->ccy, r->uy, GMP_RNDN);
    // delta^2 = |c' - c|^2
    mpfr_sub(r->ux, r->cccx, r->ccx, GMP_RNDN);
    mpfr_sub(r->uy, r->cccy, r->ccy, GMP_RNDN);
    mpfr_sqr(r->vx, r->ux, GMP_RNDN);
    mpfr_sqr(r->vy, r->uy, GMP_RNDN);
    mpfr_add(r->ux, r->vx, r->vy, GMP_RNDN);


    // enough_bits is boolean value 
    int enough_bits = 0 < 2 * (mpfr_get_prec(r->epsilon2) - r->precision) + mpfr_get_exp(r->epsilon2);

    if (enough_bits) 
     {
      // converged = delta^2 < eps^2
      int converged = mpfr_less_p(r->ux, r->epsilon2);
      if (converged) {
        // eps^2 <- |c' - r->c|^2 >> (2 * accuracy)
        mpfr_sub(r->ux, r->cccx, r->cx, GMP_RNDN);
        mpfr_sub(r->uy, r->cccy, r->cy, GMP_RNDN);
        mpfr_sqr(r->vx, r->ux, GMP_RNDN);
        mpfr_sqr(r->vy, r->uy, GMP_RNDN);
        mpfr_add(r->ux, r->vx, r->vy, GMP_RNDN);
        mpfr_div_2ui(r->epsilon2, r->ux, 2 * r->accuracy, GMP_RNDN);
        // j <- j + 1
        r->j = r->j + 1;
        // r->c <- c'
        mpfr_set(r->cx, r->cccx, GMP_RNDN);
        mpfr_set(r->cy, r->cccy, GMP_RNDN);
        return 1;
       }
     } // then
    else {
      // if not enough bits of precision then bump precision : precision = precision + 32 
      mpfr_prec_t prec = mpfr_get_prec(r->cx) + 32;
      mpfr_prec_round(r->cx, prec, GMP_RNDN);
      mpfr_prec_round(r->cy, prec, GMP_RNDN);
      mpfr_prec_round(r->epsilon2, prec, GMP_RNDN);
      mpfr_set_prec(r->ccx, prec);
      mpfr_set_prec(r->ccy, prec);
      mpfr_set_prec(r->cccx, prec);
      mpfr_set_prec(r->cccy, prec);
      mpfr_set_prec(r->zx, prec);
      mpfr_set_prec(r->zy, prec);
      mpfr_set_prec(r->dzx, prec);
      mpfr_set_prec(r->dzy, prec);
      mpfr_set_prec(r->ux, prec);
      mpfr_set_prec(r->uy, prec);
      mpfr_set_prec(r->vx, prec);
      mpfr_set_prec(r->vy, prec);
      i = 0; // start again with increased precision 
    }

    //
    mpfr_set(r->ccx, r->cccx, GMP_RNDN);
    mpfr_set(r->ccy, r->cccy, GMP_RNDN);
  }  // for (unsigned int i
  return 0;
}



// get 
extern void mandelbrot_external_ray_in_get(struct mandelbrot_external_ray_in *r, mpfr_t x, mpfr_t y) {
  mpfr_set_prec(x, mpfr_get_prec(r->cx));
  mpfr_set(x, r->cx, GMP_RNDN);
  mpfr_set_prec(y, mpfr_get_prec(r->cy));
  mpfr_set(y, r->cy, GMP_RNDN);
}




void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s angle depth\n"
    "outputs space-separated complex numbers on stdout\n"
    "example : \n %s 1/3 25\n"
    "or \n %s 1/3 25 > r.txt\n",
    progname, progname, progname);
}





// ------------------- main needs input ------------------------------------------------------
int main(int argc, char **argv) {
  
  // read and check input
  if (argc < 3) { usage(argv[0]); return 1; }
  
    
  mpq_t angle;
  mpq_init(angle);
  mpq_set_str(angle, argv[1], 0);
  mpq_canonicalize(angle);
  
  int depth = atoi(argv[2]);

  struct mandelbrot_external_ray_in *ray = mandelbrot_external_ray_in_new(angle);
 
  mpfr_t cre, cim;
  mpfr_init2(cre, 53);
  mpfr_init2(cim, 53);

  // compute and send to the output 4*depth complex points ( pair of arbitrary precision numbers )
  for (int i = 0; i < depth * 4; ++i) {
  
    mandelbrot_external_ray_in_step(ray);
    mandelbrot_external_ray_in_get(ray, cre, cim);
    // send new c to output  : (input of other program) or (text file  ) or console 
    mpfr_out_str(0, 10, 0, cre, GMP_RNDN); putchar(' ');
    mpfr_out_str(0, 10, 0, cim, GMP_RNDN); putchar('\n');
  }

  

  // clear 
  mpfr_clear(cre);
  mpfr_clear(cim);
  mandelbrot_external_ray_in_delete(ray);
  mpq_clear(angle);
 
 return 0;
}

/*

from gui/mainc

 the GUI traces rays (probably how 
it finds the landing point for periodic rays


static gpointer ray_in_thread(gpointer userdata) {
  struct ray_in_data *d = userdata;
  int period = mandelbrot_binary_angle_period(d->anno->u.ray_in.angle);
  int periodic = mandelbrot_binary_angle_preperiod(d->anno->u.ray_in.angle) == 0;
  int n = 0;
#define VARS zx, zy, cx, cy, z2, mz2, t0, t1, t2, t3
  mpfr_t VARS;
  mpfr_inits2(53, VARS, (mpfr_ptr) 0);
  while (! G.cancelled && n < 4 * G.maximum_iterations && mandelbrot_external_ray_in_step(d->anno->u.ray_in.ray)) { // FIXME library sharpness
    d->anno->u.ray_in.line = point_new(d->anno->u.ray_in.line);
    mandelbrot_external_ray_in_get(d->anno->u.ray_in.ray, d->anno->u.ray_in.line->x, d->anno->u.ray_in.line->y);
    n++;
    if (periodic) {
      if (n >= 4 * period && ! d->anno->u.ray_in.have_nucleus) { // FIXME library sharpness
        int mp = 0;
        mpfr_prec_t p = mpfr_get_prec(G.view->cx);
        mpfr_set_prec(zx, p);
        mpfr_set_prec(zy, p);
        mpfr_set_prec(cx, p);
        mpfr_set_prec(cy, p);
        mpfr_set_prec(z2, p);
        mpfr_set_prec(mz2, p);
        mpfr_set_prec(t0, p);
        mpfr_set_prec(t1, p);
        mpfr_set_prec(t2, p);
        mpfr_set_prec(t3, p);
        mpfr_set_d(zx, 0.0, GMP_RNDN);
        mpfr_set_d(zy, 0.0, GMP_RNDN);
        mpfr_set(cx, d->anno->u.ray_in.line->x, GMP_RNDN);
        mpfr_set(cy, d->anno->u.ray_in.line->y, GMP_RNDN);
        mpfr_set_d(mz2, 1.0/0.0, GMP_RNDN);
        for (int i = 0; i < period; ++i) {
          cmpfr_sqr(zx, zy, zx, zy, t0, t1, GMP_RNDN);
          cmpfr_add(zx, zy, zx, zy, cx, cy, GMP_RNDN);
          cmpfr_abs2(z2, zx, zy, t0, t1, GMP_RNDN);
          if (mpfr_less_p(z2, mz2)) {
            mp = i + 1;
            mpfr_set(mz2, z2, GMP_RNDN);
          }
        }
        if (mp == period) {
          mandelbrot_nucleus(d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, cx, cy, period, 64);
          d->anno->u.ray_in.have_nucleus = 1;
          switch (mandelbrot_shape(d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, period)) {
            case mandelbrot_shape_cardioid: {
              mpfr_set_d(zx, 1.0, GMP_RNDN);
              mpfr_set_d(zy, 0.0, GMP_RNDN);
              mpfr_t wux, wuy;
              mpfr_init2(wux, mpfr_get_prec(d->anno->u.ray_in.nucleusx));
              mpfr_init2(wuy, mpfr_get_prec(d->anno->u.ray_in.nucleusx));
              mandelbrot_interior(wux, wuy, d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, period, zx, zy, 64);
              d->anno->u.ray_in.have_landing = 1;
              mpfr_clear(wux);
              mpfr_clear(wuy);
              break;
            }
            case mandelbrot_shape_circle: {
              mpfr_t wux, wuy;
              mpfr_init2(wux, mpfr_get_prec(d->anno->u.ray_in.nucleusx));
              mpfr_init2(wuy, mpfr_get_prec(d->anno->u.ray_in.nucleusx));
              complex double t = cexp(I * 0.001);
              mpfr_set_d(zx, creal(t), GMP_RNDN);
              mpfr_set_d(zy, cimag(t), GMP_RNDN);
              mandelbrot_interior(wux, wuy, t0, t1, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, period, zx, zy, 64);
              t = cexp(I * -0.001);
              mpfr_set_d(zx, creal(t), GMP_RNDN);
              mpfr_set_d(zy, cimag(t), GMP_RNDN);
              mandelbrot_interior(wux, wuy, t2, t3, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, period, zx, zy, 64);
              cmpfr_add(d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, t0, t1, t2, t3, GMP_RNDN);
              cmpfr_mul_2si(d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, -1, GMP_RNDN);
              d->anno->u.ray_in.have_landing = 1;
              mpfr_clear(wux);
              mpfr_clear(wuy);
              break;
            }
          }
        }
      }
    }
    if (d->anno->u.ray_in.have_landing) {
      mpfr_prec_t p = mpfr_get_prec(d->anno->u.ray_in.line->x);
      mpfr_set_prec(zx, p);
      mpfr_set_prec(zy, p);
      mpfr_set_prec(z2, p);
      mpfr_set_prec(t0, p);
      mpfr_set_prec(t1, p);
      cmpfr_sub(zx, zy, d->anno->u.ray_in.line->x, d->anno->u.ray_in.line->y, d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, GMP_RNDN);
      cmpfr_abs2(z2, zx, zy, t0, t1, GMP_RNDN);
      if (mpfr_less_p(z2, d->anno->u.ray_in.eps_2)) {
        break;
      }
    }
    if (n % 100 == 0) ray_in_progress_thread();
  }
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  if (! G.cancelled) {
    add_annotation_thread(d->anno);
    free(d);
    gdk_threads_add_idle(ray_in_thread_done, 0);
  } else {
    delete_annotation(d->anno);
    free(d);
    gdk_threads_add_idle(ray_in_thread_cancelled, 0);
  }
  return 0;
}

static void start_ray_in(struct mandelbrot_binary_angle *angle) {
  struct ray_in_data *d = malloc(sizeof(struct ray_in_data));
  d->anno = malloc(sizeof(struct annotation));
  d->anno->next = 0;
  d->anno->selected = 0;
  d->anno->label = mandelbrot_binary_angle_to_string(angle);
  d->anno->tag = annotation_ray_in;
  d->anno->u.ray_in.line = 0;
  d->anno->u.ray_in.angle = angle;
  mpq_t q;
  mpq_init(q);
  mandelbrot_binary_angle_to_rational(q, angle);
  d->anno->u.ray_in.ray = mandelbrot_external_ray_in_new(q);
  mpq_clear(q);
  d->anno->u.ray_in.have_nucleus = 0;
  d->anno->u.ray_in.have_landing = 0;
  mpfr_init2(d->anno->u.ray_in.nucleusx, 53);
  mpfr_init2(d->anno->u.ray_in.nucleusy, 53);
  mpfr_init2(d->anno->u.ray_in.landingx, 53);
  mpfr_init2(d->anno->u.ray_in.landingy, 53);
  mpfr_init2(d->anno->u.ray_in.eps_2, 53);
  mpfr_set(d->anno->u.ray_in.eps_2, G.view->pixel_spacing, GMP_RNDN);
  mpfr_sqr(d->anno->u.ray_in.eps_2, d->anno->u.ray_in.eps_2, GMP_RNDN);

  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  G.dialog = gtk_dialog_new_with_buttons("Progress", GTK_WINDOW(G.window), flags, NULL, NULL);
  GtkWidget *box = gtk_dialog_get_content_area(GTK_DIALOG(G.dialog));
  G.bar = gtk_progress_bar_new();
  gtk_container_add(GTK_CONTAINER(box), G.bar);
  
  G.cancelled = 0;
  G.progress = 0;

  GThread *thread = g_thread_new("ray_in", ray_in_thread, d);
  gtk_widget_show_all(G.dialog);
  gtk_dialog_run(GTK_DIALOG(G.dialog));

  G.cancelled = 1;
  g_thread_join(thread);
  if (G.dialog) {
    gtk_widget_destroy(G.dialog);
  }
}


*/






